#include <iostream>
#include "metalist.hpp"

using namespace std;

int main(){
    using bind = basic_behavioral_map<type_list<int, string>, type_list<decltype([](int a){cout << ++a << endl;}),
                                                                        decltype([](std::string a){cout << a << endl;})>>;
    using manager = unknown_mg<type_list<toint, tostring>, bind>;
    
    unknown var;
    cin >> var;
    manager::call(var);


    using bind2 = basic_behavioral_map<type_list<int, string>, type_list<decltype([](int a, int b){cout << ++a + b << endl;}),
                                                                        decltype([](std::string a, int b){cout << a << " " << b << endl;})>>;
    using manager2 = unknown_mg<type_list<toint, tostring>, bind2>;
    
    cin >> var;
    manager2::call(var, 7);
    
}