#include <cassert>
#include <utility>
#include <functional>
#include <string>

struct empty{};

template<typename T>
using type_list_ft = T::type;

template<typename T, typename... Tp>
struct type_list{
	using type = T;
	using next = type_list_ft<type_list<Tp...>>;			
	typedef type_list<Tp...> others;
	static constexpr uint32_t size = sizeof...(Tp) + 1;
};

template<typename T>
struct type_list<T>{
	using type = T;
	typedef empty next;
	typedef type_list<empty> others;
	static constexpr uint32_t size = 1;
};




template<typename T, typename U>
struct basic_behavioral_map{
	static_assert(T::size == U::size);
	typedef T list;
	typedef U behaviors;
	template<typename V, typename list_to_use>
	static constexpr int64_t get_index(V value, uint32_t index = 0) {
		using to_check = V;
		if constexpr(std::is_same_v<type_list_ft<list_to_use>, empty>) return -1;
		else if constexpr(std::is_same_v<type_list_ft<list_to_use>, to_check>) return index;
		else return get_index<list_to_use::others>(value, ++index);
	}
	template<typename V>
	static constexpr bool exists(){
		using to_check = V;
		if constexpr(std::is_same_v<type_list_ft<list>, empty>) return false;
		else if constexpr(std::is_same_v<type_list_ft<list>, to_check>) return true;
		else return exists<list::others>();
	}
	template<typename V, typename list_to_use>
	static constexpr bool exists(){
		using to_check = V;
		if constexpr(std::is_same_v<type_list_ft<list_to_use>, empty>) return false;
		else if constexpr(std::is_same_v<type_list_ft<list_to_use>, to_check>) return true;
		else return get_index<list_to_use::others>();
	}	

	template<typename list_to_use, typename current_behavior, typename V, class... Args>
	static constexpr void p_call(V value, Args... arguments){
		using to_check = V;
		if constexpr(std::is_same_v<type_list_ft<list_to_use>, empty>) {}
		else if constexpr(std::is_same_v<type_list_ft<list_to_use>, to_check>) {type_list_ft<current_behavior> needed; std::invoke(needed, value, arguments...);}
		else p_call<typename list_to_use::others, typename current_behavior::others>(value, arguments...);
	}
	template<typename V, class... Args>
	static constexpr void call(V value, Args... arguments){
		using to_check = V;
		if constexpr(std::is_same_v<type_list_ft<list>, empty>) {}
		else if constexpr(std::is_same_v<type_list_ft<list>, to_check>) {type_list_ft<behaviors> needed; std::invoke(needed, value, arguments...);}
		else p_call<typename list::others, typename behaviors::others>(value, arguments...);
	}
};

using unknown = std::string;
using bad_unknown_cast = std::exception;

#define unknown_cf(type, ...) using to ## type = decltype([](unknown x)-> type { __VA_ARGS__ })

using tostring = decltype([](unknown x)-> std::string{ return x; });

unknown_cf(double, return stod(x););
unknown_cf(float, return stof(x););
unknown_cf(int, return stoi(x););
unknown_cf(char, if(x.size() > 1) {bad_unknown_cast err; throw err;} else return x[0];);
unknown_cf(long, return stol(x););
unknown_cf(uint64_t, return stoul(x););
unknown_cf(bool, if(x=="true" || x=="True" || x=="TRUE" || x=="1") return true; else if(x=="false" || x == "False" || x == "FALSE" || x=="0") return false; else{ bad_unknown_cast err; throw err;});


/*template<typename T>
struct type_container{
	using type = T;
};*/

/*template<typename T>
using type_get<T> = T::type;*/

struct not_in_list{};

template<typename _convert_methods, typename behavioral_map>
struct unknown_mg{
	using list = behavioral_map::list;
	using convert_methods = _convert_methods;
	using behaviors = behavioral_map::behaviors;
	
	template<class... Args>
	static void call(unknown value, Args... arguments){
		try{
			type_list_ft<convert_methods> to_call;
			std::invoke_result_t<type_list_ft<convert_methods>, unknown> x = std::invoke(to_call, value);
			behavioral_map::call(x, arguments...);
			return;
		} catch(std::exception& e) {
			if constexpr(list::size == 1) return;
			else call<typename list::others, typename convert_methods::others>(value, arguments...);
		}
		return;
	}
	template<typename list_to_use, typename remaining_conversions,  class... Args>
	static void call(unknown value, Args... arguments){
		try{
			type_list_ft<remaining_conversions> to_call;
			std::invoke_result_t<type_list_ft<remaining_conversions>, unknown> x = std::invoke(to_call, value);
			behavioral_map::call(x, arguments...);
			return;
		} catch(std::exception& e) {
			if constexpr(list_to_use::size == 1) return;
			else call<typename list_to_use::others, typename remaining_conversions::others>(value, arguments...);
		}
		return;
	}

};
